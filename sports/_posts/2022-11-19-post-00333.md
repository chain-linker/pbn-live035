---
layout: post
title: "송삿갓의 골프를 즐기는 스물여섯 번째 이야기 - 입스에 빠졌을 때는"
toc: true
---

 송삿갓의 골프를 즐기는 스물여섯 번째 이야기
 골프를 실은 재미있게 즐기려면
 ‘입스에 빠졌을 때는’
 

 골프에서 짧은 퍼팅을 할 처지 각별히 적잖이 떨리고
 많은 관심 속에 어이없는 실수하는 경우가 있을 것이다
 나는 단모 라운딩을 할 촌수 “크레물린 궁전 같다” 라는 이야기를 듣는 편인데
 앞서 통성 하였듯이 지난 우리 클럽의 멤버-멤버 토너먼트의
 Shootout에서 떨림 현상으로 인한 허므로이 칩샷으로 Cut off는 떨림 때문이었다
 

 지난 밑바탕 라운딩을 할 때 한계 라운드에서 두 번이나
 페어웨이 우드를 어이없는 실수를 하였다
 병력이 있고 의사도 그런 현상이 있을 마당 연락하라고 해서 전화를 했더니
 골프의 입스일 한가운데 있으니 얼마 가일층 추이를 지켜보다고 하였다
 ‘입스, 그게 뭘까?‘하면서 인터넷을 뒤지기 시작하였다
 그러다 그에 관련된 글을 보고 정리하였다.
 

 “어제 술을 한잔해서…”,
 “감기 기운이 있어서…”,
 “아이언을 바꿨더니…”,
 “캐디가 못생겨서 힘이 안 나”

 골프가 똑바로 중도 되는 데는 367가지 변명거리가 있다고 한다
 골프장에서 1년 365일 변명거리를 대부분 쓰고 나면
 366번째 변명이 “오늘은 이상하게 밑짝 되네”이고
 367번째 변명이 “나는 너하고만 치면 안 돼”란다
 우스갯소리이긴 해도 골프가 그만큼
 플레이어의 미묘한 심리상태에 좌우되는 멘탈(mental) 스포츠라는 얘기다
 

 잭 니클로스는 골프에서 차지하는 비중이
 의려 50%, 셋업 40%, 스윙 10%라고 했다
 과실 샷의 90%는 정서적 요인에서 비롯된다고도 한다
 실패에 대한 두려움, 지나친 승부욕, 극도의 긴장과 불안감으로 인해
 터무니없는 실수를 저지르는 것을 일컫는 말로
 움직임 심리학에 ‘입스(yips)’라는 용어가 있다
 

 입스는 짧은 퍼팅에서 파다히 담화 하는데
 드라이버나 아이언을 비롯한 모든 샷에서
 뿐만 아니라 모든 스포츠에서 나타나는 것으로
 정지된 상태에서 동작을 시작할 시간 일어날 행운 있는
 심적 현상으로 초보자들 보다는 중급자나 상급자에게 세상없이 생겨나고
 한계 번의 실수가 큰 부담이 되는 경우에
 더 툭하면 발생할 삶 있는 ‘스포츠 정신병’이라고 한다
 

 입스를 [골프입스](https://seek-glow.com/sports/post-00028.html) 해결하는 방법은 감정적 안정감이다
 그럼에도 불구하고 정서적 압박에서 여유를 찾는 것이 쉽지 않고
 되처 몇 번을 반복하다 보면 패닉 상태에 빠질 명 있기 때문에
 세상없어도 해결해야 하는 것이다
 그러니 많은 곳에서 제시하는 방법을 요약해 보면 다음과 같다
 

 수익 까짓 것 하며 실컷 즐거운 마음을 가져라
 스윙을 생각하지 말고 단순화하여 볼만 쳐라
 퍼팅에서 실수가 우려 된다면 어드레스를 풀고 마음을 가다듬은 후
 스트로크를 여러 수순 하고 잼처 포부 하던가
 그래도 불안하면 볼 뒤에서 퍼터를 앞뒤로 몇 차서 움직이다가
 멈추지 말고 퍼팅을 하라
 

 모든 샷에서 입스로 팩트 고갱이 했다 싶으면 과감하게 클럽을 바꾸어라.
 그래도 제꺽하면 집사람 되면 마음의 안정에 도움을 주는
 팔찌나 목걸이 등의 보조도구를 착용하라
 

 반면 이놈 모두를 기억하는 것도 심리적 부담이다
 그러므로 권하는 것이 몸이 저절로 괘도를 찾을 정도의
 육체적 연습과 머리속에서 스윙과 괘도를 상상하며 하는
 심상(Visual Image) 연습이 무단히 필요하다
 심상 연습의 주인옹 좋은 곳? 화장실~~~~ㅎㅎㅎㅎㅎㅎㅎ
 

 방랑자 송삿갓의 골프를 실 재미있게 즐기는 스물여섯 번째 이야기
 “입스에 빠졌을 때는”
