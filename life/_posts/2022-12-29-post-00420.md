---
layout: post
title: "동탄 파크릭스 1차(A51-1BL / A51-2BL / A52BL) 분양가 및 인근 시세 분석"
toc: true
---

 근일 주택시장에서 ‘공세권’, ‘숲세권’ 등 쾌적한 주거환경을 갖춘 단지의 인기가 날로 커지고 있습니다. 녹지가 제공하는 미세먼지 저감, 폭염 완화 효과를 기대할 수 있는 데다 코로나19 여파로 천체 속에서 답답함을 해소하고 휴식을 취하려는 수요자들이 세상없이 늘어난 이유라고 생각합니다.
이런한 감념 그렇게 업계에서는 미세먼지, 코로나19 등이 맞물리면서, 쾌적한 환경에서 생활하려는 수요층이 다양한 세대로 확산되고 있습니다. 영별히 요사이 사용되다 젊은 세대에서도 건강에 대한 관심이 크게 증가하고 있는 데다 높은 삶의 질을 추구하려는 경향은 세대를 막론하고 커지고 있어 우주 선린 단지의 인기가 한결 높아졌습니다.
 시고로 짬 현대건설·계룡건설산업·동부건설·대보건설 4개 건설사가 시공을 맡은 ‘동탄 파크릭스’의 분양일정이 공개됐습니다. 총 4개 단지가 공급될 예정인데요. 이참 1차 분양에는 3개단지를 분양하고, 마지막 2차 분양으로 1개 단지만 외따로 분양을 한다고하네요.
오늘은 ‘동탄 파크릭스’ 3개단지의 분양가와 청약정보 함께 보겠습니다.
## 1. 사업개요

'동탄 파크릭스'는 총 4개 블록, 지하 2층~지상 20층, 44개 동, 전용면적 74~110㎡로 구성되며 A51-1블록 310세대, A51-2블록 414세대, A52블록 679세대, A55블록 660세대 총 2,063세대로 조성될 예정입니다. 수익 중급 A51-1블록, A51-2블록, A52블록 1,403세대를 1차로 분양 합니다.
입지 여건은 비슷하지만, 개인적으로 4개 블럭 중에서 바깥양반 좋아보이는 단지는 A55블럭이 아닐까 생각합니다. 전체 공감하시겠지만 학교를 끼고 있고 남쪽으로 녹지공간과 학교가 있어 일조권과 조망권 확보가 방금 될 것으로 예상됩니다.
## 2. 입지환경

 동탄 파크릭스는 반경 5km 내에 SRT 동탄역이 위치해 있습니다. 도보로는 동탄역까지 접근하기 어렵지만, 버스 혹은 자동차를 이용해 동탄역까지만 접근한다면 SRT를 이용해 수서역까지 약 17분에 이동할 수 있어 한복판 접근성이 우수합니다. 또한 SRT 동탄역에 정차 예정인 GTX-A노선이 계획대로 2024년 트리핑 한다면 추가적인 말 상승이 일어날 것이라고 생각됩니다.
동탄 파크릭스는 상록GC, 기흥CC 등에 둘러싸여 있습니다. 상록GC의 실용례 자연지형, 수목, 바위 등 산악지형의 영적 그대로를 살려낸 친환경 골프장이며, 기흥CC의 사정 약 246만㎡ 면적으로 중심 여의도공원 면적의 약 10배에 달하는 규모로 조성돼 있어 쾌적한 자연환경을 자랑합니다.

또 신주거문화타운을 가로지르는 신리천이 인접해 있으며 신리제1저수지, 신리제2저수지 등 수변이 가깝으며, 이익금 밖에 왕배산3호공원 등이 위치해 있어 산책, 변천 등을 즐길 수 있습니다.

## 3. 분양가
 (※ 층별 자세한 분양가는 아래쪽 첨부된 입주자 모집공고를 확인해주세요)
국평인 전용면적 84타입이 [동탄 파크릭스](https://screwslippery.com/life/post-00035.html) 5억 초반대로 근처 시세대비 분양가는 낮은 편입니다. 하지만, 시재 하락 추세로 지금보다 더더욱 하락할 고갱이 있다는 점이 변수인데요. 미래는 누구도 모르지만 동탄의 때 꽤 꽤 하락했기 그렇게 향후 한층 하락한다 해도 약간의 조정만 있을 뿐 너무너무 큰 하락은 없을 것으로 생각됩니다.
 곁 신축단지 시세입니다. 5억 대에 거래된 것도 있으나, 직전 거래와 가격차이가 사뭇 납니다. 이해관계인 거래일 복 있으며, 통상 시세를 본다면 6억 초반이 맞을 것 같습니다. 시세가 6억 초반이지만, 미리 생활기반시설이 갖춰진 곳과 파크릭스와의 단일 비교는 중과 어렵겠네요. 예사 기반시설이 갖추어진 지역과 입주 초반인 지역의 시세차이는 어느정도 나기 왜냐하면 입주초기에는 파크릭스의 시세가 좌측에 있는 단지들보다 우극 낮게 책정될 것으로 보이는데요.
그렇다면 동탄 프크릭스의 분양가와 시세를 비교해 봤을 때, 안전마진이 별로 없어보이긴 하네요.
시세는 6억인데, 분양가는 5억 초반, 근데 기반시설은 안갖춰져 있고 확장비와 옵션비 등을 추가적으로 내야되기 그리하여 큰 메리트는 없지 않을까 생각합니다.
다만, 쾌적하고, 조용하고, 가족들 차량이 있다면 만분 괜찮은 선택지로 보여지고요.

## 4. 청약일정
 ▶입주자모집공고: 22. 11. 04.
▶특별공급: 22. 11. 14.
▶일반1순위: 22. 11. 15.
▶일반2순위: 22. 11. 16.
▶당첨자발표: 22. 11. 22.

## 5. 마치며...
 저는 개인적으로 숲, 강, 바다 등 자연친화적인 입지를 좋아합니다. 물론, 생활기반시설과 교통여건이 좋으면서 자연환경까지 좋다면 더욱 좋겠죠. 하지만, 동탄 파크릭스는 쾌적한 배경 외에는 시간이 무장 필요할 것 같습니다.
동탄2신도시 외곽인 점과 교통이 불편한 점, 아울러 기반시설이 갖춰져 있지 않은 점 등 현재는 아쉬운 점이 한층 많다고 느껴지네요. 더욱이 지도자 중요한 분양가인데요. "싸다" 라는 생각이 들지 않고, 애매한 포지션의 분양가로 청약 대기자들이 몰릴지 의문이 생기네요.
 2022.11.03 - [부동산/분양정보] - 둔산 더샵 엘리프 분양가 및 청약정보 분석
 2022.11.02 - [부동산] - 인천 영종도 부동산 분석(재건축 & 오션뷰 신축 투자)
 2022.10.19 - [부동산/재건축재개발] - 인천 재개발 & 재건축 진행현황(엑셀_excel 파일 첨부)

