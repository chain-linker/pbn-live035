---
layout: post
title: "LAH 국산 경무장헬기"
toc: true
---


## ◎LAH 국산 경무장헬기
 

 

 KAI
 원래는 500MD와 BO-105헬기 대체를 위한 사업이었으나, 어느 전순간 AH-1S까지 대체하는 사업이 되었다.
 ​
 처음에는 공격형은 스키드, 민수용은 H155의 방식을 채택하였으나 개발과정에서 같은 바퀴식 착륙방식으로 바뀌었다.
 ​
 ▷개발
 *주관기관
 대한민국 육군, 국방과학연구소, KAI, 지식경제부
 ​
 *개발목적
 국산공격헬기 개발을 위한 기초를 다지고, 노후화된 500MD와 코브라헬기를 대체하며, 국산헬기 개발과 항공부품의 국산화기반 조성이 해당사업의 목적이다.
 ​
 *기간​
 2015.06.~2022.
 ​
 *사업비
 총사업비는 산업부(3500억원)와 KAI(2000억원),유로콥터(4000억원)가 분담한다.​
 ​
 ​
 *참여업체​
 일통 : KAI/EC
 기술협력 : 에어버스 헬리콥터스
 시스템통합 및 전자장비 : 한화
 여잉 : 단암시스템즈, 휴니드시스템즈, S&T중공업
 ​
 *예상생산수량​
 200대
 ​
 *전임기
 BO-105(12), AH-1S(70?), 500MD(180?)​
 ​
 *시제기
 공격형 3기, 민수용 2기(1대는 프랑스 에어버스사 제작)
 실증기 1기(HL9649)
 ​
 *경과(국방일보)​
 2009. 4. 소요결정(제240차 합동참모회의)
 2010. 7. 사업타당성 조사(軍 산보 충족, 경제성 확보가능)
 2010. 12. 사업추진기본전략 의결(제47회 방위사업추진위원회)
 2011~12. 국방과학연구소 탐색개발 수행
 2013. 11. 민·군수 헬기 연계개발계획 의결(제8차 항우심)
 2014. 2. 체계개발기본계획 의결(제75회 방추위)
 2014. 7. 협상대상업체/우선순위결정결과 보고(제81회 방추위)
 2015. 6. 체계개발실행계획서 확정/계약체결(한국항공)
 2015. 7. 영업 착수회의
 2015. 12. 체계요구조건검토(SRR)/체계기능검토(SFR)
 2016. 5. 소프트웨어요구조건검토(SSR)
 2016. 6. 체계개발 착수
 2016. 8. 기본설계 검토(PDR) 및 종료
 2016. 11. 시제1호기 부품 해산 착수함
 (조립은 기본설계에 따른 것으로, 조립 과정에서 엔진을 포함한 중대 구성품의 상세설계를 진행)
 2017. 6. 시제1호기 조립 착수
 2017. 10. 상세설계가 성능 목표를 충족하는지 검토
 2018. 12. 시제1호기 공개
 2020.04. 모터 지상시험
 2019. 07. 시제1호기 초도 비행
 *자료 : 방위사업청
 ※사업에 대한 필자의 의견
 https://m.blog.naver.com/kevin1406/222056807630
 

 ▷임무​
 무장정찰헬기로서 500MD를 대체하고, 지상군엄호, VIP 및 물자이송을 수행하는 것이 임무이다.
 ​
 ▷제원​
 *탑승인원
 승무원 : 2명
 무장병력 : 5명(?)
 ​
 *크기​
 동체전장 : 14.1m
 날개 포함전장 : 13.1m
 전폭 : 2.2m(스티브윙 포함시 3.48m)
 석일 : 4.3m
 마스트 높이 : 3.6m
 로터 지름 : 12.6m
 테일로터부 지름 : 1100mm
 ​
 ​
 유상하중 : 2301kg
 슬링시 : N/A
 공허중량 : N/A
 최대이륙중량 : 4,920kg
 ​
 *엔진​
 Turbomeca Arriel 2L2x2
 1,032hp (615kW)
 ​
 *비행성능​
 최대한도 순항속도 : 263km/h
 최고속도 : 324 km/h
 항속거리 [무인 복합기](https://imagetojpg.com/life/post-00014.html) : N/A
 최대항속거리 : 905 km
 실용상승한도 : 4572m
 비행시간 : 4시간 3분
 상승률 : 8.9 m/s (1,750 ft/min)
 ​
 ▷엔진​
 *Turbomeca Arriel 2L2x2
 

 사프란사와 한화에서 공동으로 개발하고 한화에서 납품하는 헬기용 엔진이다.
 ​
 추력 : 1,032hp
 출력 : 770.3kW
 중량 : 135kg
 출력비 : 3.47shp/lb
 압축비 : 9.0
 치수 : 1015×498mm(길이×직경)
 ​
 민수용과 달리 군용인증이 필요하나, 군용 인증이 되지 않은 관계로 스크린을 흡입구에 설치하였다.
 게다가 피탐률을 낮추기 위해 엔진덕트를 꺾어 올렸다.
 ​
 *전력공급체계의 케이블과 배선은 휴니드사에서 담당한다.​
 ​
 *로터블레이드
 5엽블레이드를 채용했고, 국산화하였다고 한다.
 ​
 ▷동체구조​
 H155B2의 프레임을 사용한다.
 동체바닥에는 연료탱크가 5개 내지 6개가 설치되어 있고, 케빈에 기관포탄 탄통이 설치되어 있어서 기관포까지 급탄이 되도록 되어 있다.
 ​
 LCH보다 좁은 케빈이며, 줄어든 구역에는 항전장비가 탑재된다.
 ​
 조종석과 엔진부분에 방탄설계가 적용되어 있으며, 7.62mm수준으로 알려져있다.
 ​
 설계를 매우 그대로여서인지, 수리온의 비판점을 받아들였는지는 모르겠으나, 조종석을 뒤의 캐빈으로 잡아당길 수명 있는 구조이다.
 ​
 ▷센서 및 생존장비​
 ​
 *항공기 통합생존체계
 

 한화에서 납품하며, 센서들의 정보는 스마트 다기능시현기에서 시현된다. 향후 국산 DIRCM과 RF재머가 장착될 예정이다.
 ​
 *ALE-47 CHAFF/FLARE (CMDS)
 

 BAE시스템즈에서 납품한다. 적외선 및 레이더 축적 미사일 접근시 강력한 신호를 발사하여 교란시키는 채프와 플레어를 뿌리는 장비이다.
 ​
 *RWR, LWT, MWR​
 AAR-60 MILDS MWR
 휴니드에서 납품하면 미사일 탐지장비로 앞뒤로 2개씩 총 4개가 설치되어 미사일의 접근을 탐지하는 센서들이다.
 SA-16 중적외선 대역 적외선 탐지 지대공 미사일에 대응할 수 있다.
 RWR
 LWR
 AAR-60 MILDS MWR을 제외하고 한화에서 납품하며, 적의 레이다 위협무기로부터 발사되는 레이다신호를 탐지하여 위협정보를 항공기 생존체계로 전송하는 레이다경보 센서와 LWR, MWR, RWR 등 생존장비로부터 탐지된 위협을 통합관리하는 전자전통합컴퓨터가 통합된 장비이다.
 

 RWR은 노생 전방의 TADL살짝 뒤에 동체 옆면에 설치되어 있고, LWR과 MWR은 동체측면 케빈도어 와 콕핏 도어 사이에 위아래로 양면에 설치되어 있다. 그리고 테일로터 페어링 맨뒤에 MWR, LWR, RWR이 위에서 부터 차래로 2기씩 장착되어 있다.
 ​
 *표적획득지시장비​
 (TADS : Target Acquisition Designation System)
 한화에서 납품하는 장비로 주·야간 표적을 탐지 및 추적하고, 레이저를 조사하여 공대지 유도탄, 로켓, 기총 사격을 위한 표적정보를 제공하는 전자광학 표적획득·지시 장비이다.
 해상도는 1280×1024급이다.
 항공기 노즈 부분에 구동부가 위로, 탐색부가 아래로 전시 해서 기총위에 장착되어 있다.
 ​
 ​
 ▷전술데이터링크 및 통신​
 *LINK-K
 한화에서 주도 조인 LINK-K에 통합되며, 2차형에 통합될 것으로 예상되며, MLDS를 장착할 것으로 예상된다.​
 ​
 *Ethernet Switch
 ARINC664(에어버스의 안전정보 네트워크)와 IEEE 802.3(이더넷)을 지원하며, 항공기의 시스템간의 데이터
 전송을 위해 고정된 현실 폭을 보장하여
 실시간 전달/분배하는 장치이다. 단암시스템이 공급한다.​
 ​
 *지상전술데이터링크 처리기 KVMF DLP
 ATCIS 및 B2CS와 전술정보를 공유하여 지상무기체계 거리 효과적인 작전수행을 지원하고, 아군 및 정곡 자료 등 전장상황정보를 이이 표준지도에 근 실시간으로 전시하여 전장 상황인식 능력을 보장하는 장비이다.​
 ​
 *음성접속장치AIU
 무전기를 이용한 외부통신, 사용자간 내부통신 및 항법 오디오 연동 기능을 제공하고, 통신장비를 제어하는 장비이다.
 ​
 ▷조종석
 글라스 콕핏이 적용되었으며, 방탄 처리가 되어 있다.
 ​
 *400 HMSD Scorpion
 탈레스사의 HMD로 주야간 비행을 지원하고, 과녁 재료 등을 헬멧 바이저에 시현해 준다.
 ​
 *스마트 다기능시현기(SMFD)
 한화에서 납품하며, 조종실 계기패널에 장착이 되어 비행정보 및 임무정보를 시현하고 자세 연산·제어 기능을 통해 임무컴퓨터 역할까지 수행하는 장비이다.
 ​
 *스마트 통제시현장치(SCDU)
 조종실 기미 중앙콘솔에 장착이 되어 비행 및 임무정보에 대한 진상 인풋 및 시현하는 장비이다.
 ​
 *항공기 경고 시스템
 휴니드사에서 납품한다.​
 ​
 ▷무장​
 *천검
 

 한화에서 개발하고 있는 공대지 대잔차미사일이다.
https://bloghyeonhmilitary.tistory.com/m/60

 ​
 *LOGIR(신기전로켓)
 

 70mm직경의 로켓포로 구식기동차량 및 보병살상용으로 쓰일 것으로 보인다. 7연장 터렛에서 발사된다.
 ​
 *S&T Dynamics 20mm 기관포
 

 S&T중공업에서 개발 중인 LAH용 회전터렛에 장착된 20mm기관포이다. 3열의 총신이 포함된다.
 ​
 ​
 ▷공격방식​
 TADL과 여러 센서에서 수집된 정보는 EWC를 통해 수집되어 중앙컴퓨터에 전송되고, 중앙컴퓨터는 LINK-K와 KVMF에서 수집된 정보와 취합하여, 조종사에게 전달하면 조종사가 무장을 사용하는 방식이다.
 ​
 ▷비판
 당금 미군의 상황 OH-58헬기를 퇴역시킨 나중 신형공격헬기와 아파치로 도무지 중인데, 이는 아파치의 시선 역할을 하던 OH-58이 필요가 없어졌고, 아울러 아파치가 위력정찰을하는 것이 생존과 화력에서 우수했기 때문이다.
 ​
 시고로 상황에 전세계적으로 험한 전장환경을 자랑하는 대한민국이 그저 OH-58과 같은 정찰헬기가 필요하며, 동시에 전문공격헬기인 코브라 공격헬기의 임무를 무장력과 방탄 능력, 생존성에서 뒤쳐지는 기종을 도입하는 것에 대한 비난이 많다.
 ​
 하지만, 500MD대체만 놓고 보면 확실한 전력이기에 육군의 요구조건을 어느정도 충족한 것으로 보이나, 코브라 헬기의 대체라는 목적은 두고두고 비난받아 마땅하다.
 ​
 민수용으로서의 전망이 어두운데도, 사업진행을 위해 외국 시장에서 동급기체중 35%를 차지하겠다고 과도한 홍보로 물의를 빚었다.
 ​
 ▷전망
 앞으로 KVMF와 LINK-K 등을 통해 두대의 무인기를 통제하는 방식으로의 발전이 고려되고 있고, 연구중이다.
 뿐만 아니라 향후 중국의 Z-19사례처럼 전문공격헬기로의 순차적 발전도 기대된다.​
 추가적으로 우리군은 해당기체에 무인기를 12기가량 탑재하여 다른 곳에서 이들의 정보를 바탕으로 공격하는 방식을 개발하고자 하는 것으로 보이며, 캐빈에 이들 무인기를 탑재할 생각이라고 한다.(공격형헬기는 공간이 없다나 뭐라나....그럼 미군의 신형 전투헬기는 뭐지?)
 필자는 무인기 사출시 로터와의 충돌로 추락사고가 우려된다.​
 ​
 ▷정찰용 유무인 복합기형상
 2020년 오밀리터리에서 공개된 영상을 분석해보면, 이스라엘의 하피형 국산무인기 4대를 대전차 미사일 대신 발사대에 장착하고, 동체에 운용콘솔을 설치하여 운용할 것으로 보인다.
 ​
 다만, 2가지형으로 개발 될 서울 있을 것으로 보이는데, 1개는 MH-60의 소노부이처럼 무인기 9대를 옆구리로 발사하는 방식이고, 다른 하나는 위에서 설명한 형상이다.
 ​
 

 

 소노부이?
 

 

 무인기는 이쁘둥이 EO/IR과 을 내장하고 EFP탄두나 성형작약탄을 장착할 것으로 사료된다.​
 ​
 ▷기타
 보탬 사업에 대한 계약에 의해 KAI가 앞으로 모든 H155계열기를 사천 공장에서 생산하게 된다.
 ​
 정보출처
 구글이미지검색
 네이버 블로그 쉘든의 밀리터리
 네이버 블로그
 휴니드
 http://www.huneed.co.kr/kor/02_business/2016_business16.asp
 http://www.huneed.co.kr/kor/02_business/2016_business17.asp
 한화
 https://www.hanwhasystems.com/m/kr/business/defense/5/2/view.do
 https://n.news.naver.com/article/011/0003812603
 youtu.be/jSnqSW4AyYE
 

